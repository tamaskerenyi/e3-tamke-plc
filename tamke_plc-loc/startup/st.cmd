# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require tamke_plc, plcfactory

# -----------------------------------------------------------------------------
# Utgard-Lab
# -----------------------------------------------------------------------------
epicsEnvSet(IPADDR,       "172.30.244.37")
epicsEnvSet(RECVTIMEOUT,  3000)
epicsEnvSet(SAVEFILE_DIR, "/var/log/tamke_plc")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
iocshLoad("$(tamke_plc_DIR)tamke_plc.iocsh", "IPADDR=$(IPADDR), RECVTIMEOUT=$(RECVTIMEOUT), SAVEFILE_DIR=$(SAVEFILE_DIR)")

iocInit
